//
//  String+Trim.swift
//  github-search
//
//  Created by Anna Fortuna on 22/02/2016.
//  Copyright © 2016 AVF. All rights reserved.
//

import Foundation

extension String {
  func trim() -> String {
    return self.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
  }
}