//
//  GlobalSplitViewController.swift
//  github-search
//
//  Created by Anna Fortuna on 25/02/2016.
//  Copyright © 2016 AVF. All rights reserved.
//

import Foundation
import UIKit

class GlobalSplitViewController: UISplitViewController, UISplitViewControllerDelegate {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    self.delegate = self
  }
  
  func splitViewController(splitViewController: UISplitViewController, collapseSecondaryViewController secondaryViewController: UIViewController, ontoPrimaryViewController primaryViewController: UIViewController) -> Bool{
    return true
  }
}