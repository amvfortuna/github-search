//
//  MasterViewController.swift
//  github-search
//
//  Created by Anna Fortuna on 21/02/2016.
//  Copyright © 2016 AVF. All rights reserved.
//

import UIKit

class MasterViewController: UITableViewController {
  
  // MARK: - Properties
  var detailViewController: DetailViewController? = nil
  var requestModel = RequestModel()
  var results = [AnyObject]()
  var isSearching = false
  var searchingBySearchBar = false
  let searchController = UISearchController(searchResultsController: nil)
  let spinner = UIActivityIndicatorView(activityIndicatorStyle: .WhiteLarge)
  
  // MARK: - View Setup
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    if let split = self.splitViewController {
      let controllers = split.viewControllers
      self.detailViewController = (controllers[controllers.count-1] as! UINavigationController).topViewController as? DetailViewController
    }
    
    definesPresentationContext = true
    
    self.tableView.tableHeaderView = self.searchController.searchBar
    
    searchController.dimsBackgroundDuringPresentation = false
    searchController.searchBar.delegate = self
    searchController.searchBar.placeholder = "E.g. Swift, Objective-C"
    searchController.searchBar.barTintColor = UIColor(colorLiteralRed: (245/255), green: (245/255), blue: (245/255), alpha: 1)
    
    spinner.tintColor = UIColor.blackColor()
    spinner.startAnimating()
  }
  
  override func viewWillAppear(animated: Bool) {
    self.clearsSelectionOnViewWillAppear = self.splitViewController!.collapsed
    super.viewWillAppear(animated)
  }
  
  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  // MARK: - Segues
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "showDetail" {
      if let indexPath = self.tableView.indexPathForSelectedRow {
        let item = results[indexPath.row]
        let controller = (segue.destinationViewController as! UINavigationController).topViewController as! DetailViewController
        controller.detailItem = item as? Repository
        controller.navigationItem.leftBarButtonItem = self.splitViewController?.displayModeButtonItem()
        controller.navigationItem.leftItemsSupplementBackButton = true
      }
    }
    else if segue.identifier == "showNotice" {
      let notice = segue.destinationViewController as! NoticeViewController
      notice.delegate = self
    }
  }
  
  // MARK: - Table View
  
  override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if isSearching {
      return 1
    }
    else {
      if results.count == 0 {
        return 1
      }
      else {
        return results.count
      }
    }
  }
  
  override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
    if isSearching {
      let cell = tableView.dequeueReusableCellWithIdentifier("searching", forIndexPath: indexPath)
      cell.userInteractionEnabled = false
      return cell
    }
    else {
      if results.count == 0 {
        if requestModel.searchKeyword != "" {
          let cell = tableView.dequeueReusableCellWithIdentifier("errorCell", forIndexPath: indexPath)
          cell.textLabel?.text = "No results found."
          cell.userInteractionEnabled = false
          return cell
        }
        else {
          let cell = tableView.dequeueReusableCellWithIdentifier("infoCell", forIndexPath: indexPath)
          cell.userInteractionEnabled = false
          return cell
        }
      }
      else {
        let data = results[indexPath.row]
        
        if let errorString = (data as? String)?.trim() {
          let cell = tableView.dequeueReusableCellWithIdentifier("errorCell", forIndexPath: indexPath)
          cell.textLabel?.text = errorString
          cell.userInteractionEnabled = false
          return cell
        }
        else {
          let cell = tableView.dequeueReusableCellWithIdentifier("searchResult", forIndexPath: indexPath) as! RepositoryCell
          let item = data as! Repository
          cell.repoNumber.text = "\(indexPath.row + 1)"
          cell.repoName.text = item.fullName
          
          if let desc = item.desc?.trim() {
            cell.repoDescription.text = desc == "" ? "No description provided." : desc
          }
          else {
            cell.repoDescription.text = "No description provided."
          }
          
          cell.userInteractionEnabled = true
          
          // We need to check if the user is almost at the bottom of the table. If so, let's begin retrieving
          // new set of data.
          if indexPath.row + 10 >= self.results.count {
            self.retrieveData(searchController.searchBar.text!)
          }
          
          return cell
        }
      }
    }
  }
  
  override func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    
    return 100
  }
  
  override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    
    return UITableViewAutomaticDimension
  }
  
  override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView(frame: CGRectZero)
  }
  
  // MARK: - Data retrieval
  func retrieveData(keyword: String) {
    if keyword.trim() != "" {
      requestModel.searchRepositories(keyword.trim(), completion: {
        
        data, error in
        
        if error != nil {
          if error!.lowercaseString.rangeOfString("api rate limit exceeded") != nil {
            dispatch_async(dispatch_get_main_queue(), {
              if self.presentedViewController == nil {
                self.performSegueWithIdentifier("showNotice", sender: nil)
              }
            })
          }
          
          print("Error retrieving data: \(error!)")
        }
        else {
          if data != nil {
            var currentResultCount = self.results.count
            var idxPaths = [NSIndexPath]()
            for item in data! {
              idxPaths.append(NSIndexPath(forRow: currentResultCount, inSection: 0))
              self.results.append(item)
              ++currentResultCount
            }
            
            dispatch_async(dispatch_get_main_queue(), {
              self.tableView.beginUpdates()
              self.tableView.insertRowsAtIndexPaths(idxPaths, withRowAnimation: .Bottom)
              self.tableView.endUpdates()
            })
          }
          else {
            print("No more items to show")
          }
        }
      })
    }
  }
}

// MARK: - Search bar delegates
extension MasterViewController: UISearchBarDelegate {
  func searchBarSearchButtonClicked(searchBar: UISearchBar) {
    let trimmedKeyword = searchController.searchBar.text!.trim()
    if trimmedKeyword != "" && trimmedKeyword != requestModel.searchKeyword {
      // Clear the table of previous results
      isSearching = true
      tableView.reloadData() // Shows the "Loading" cell
      requestModel.searchKeyword = trimmedKeyword
      results = [AnyObject]()
      
      searchController.searchBar.resignFirstResponder()
      
      requestModel.searchRepositories(trimmedKeyword, completion: {
        data, error in
        
        self.isSearching = false
        
        if error != nil {
          if error!.lowercaseString.rangeOfString("api rate limit exceeded") != nil {
            dispatch_async(dispatch_get_main_queue(), {
              if self.presentedViewController == nil {
                self.searchingBySearchBar = true
                self.performSegueWithIdentifier("showNotice", sender: nil)
              }
            })
          }
          else {
            dispatch_async(dispatch_get_main_queue(), {
              self.results.append(error!)
              self.tableView.reloadData()
            })
          }
        }
        else {
          if data != nil {
            self.results = data!
            dispatch_async(dispatch_get_main_queue(), {
              self.tableView.hidden = false
              self.tableView.reloadData()
            })
          }
        }
      })
    }
  }
  
  func searchBarCancelButtonClicked(searchBar: UISearchBar) {
    isSearching = false
    requestModel.searchKeyword = ""
    
    if results.count == 0 {
      tableView.reloadData()
    }
    else if results.count == 1 {
      if let _ = results[0] as? String {
        results = [AnyObject]()
        tableView.reloadData()
      }
    }
  }
}

// MARK: - Notice view delegate

extension MasterViewController: NoticeViewControllerDelegate {
  func dismissNoticeViewController(controller: NoticeViewController) {
    controller.dismissViewControllerAnimated(true, completion: nil)
    
    // Resume the search.
    if searchingBySearchBar {
      self.searchBarSearchButtonClicked(searchController.searchBar)
    }
    else {
      self.retrieveData(searchController.searchBar.text!)
    }
  }
}