//
//  Repository.swift
//  github-search
//
//  Created by Anna Fortuna on 21/02/2016.
//  Copyright © 2016 AVF. All rights reserved.
//

import Foundation

class Repository: NSObject {
  var name: String
  var fullName: String
  var desc: String?
  var issueURL: String
  var contributorURL: String
  
  override init() {
    self.name = ""
    self.fullName = ""
    self.desc = ""
    self.issueURL = ""
    self.contributorURL = ""
    
    super.init()
  }
}