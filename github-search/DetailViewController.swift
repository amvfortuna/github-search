//
//  DetailViewController.swift
//  github-search
//
//  Created by Anna Fortuna on 21/02/2016.
//  Copyright © 2016 AVF. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
  
  // MARK: - Properties
  @IBOutlet weak var repositoryName: UILabel!
  @IBOutlet weak var repositoryDescription: UILabel!
  @IBOutlet weak var categorySelector: UISegmentedControl!
  @IBOutlet weak var dataTable: UITableView!
  @IBOutlet weak var spinner: UIActivityIndicatorView!
  @IBOutlet private weak var dataTableHeight: NSLayoutConstraint!
  @IBOutlet weak var coverView: UIView!
  
  var issues = [AnyObject]()
  var contributors = [AnyObject]()
  var errors: [AnyObject]?
  var detailItem: Repository? {
    didSet {
      // Update the view.
      self.configureView()
    }
  }
  private var issuesRetrieved = false
  private var contributorsRetrieved = false
  private var isReloading = false
  private var hasError = false
  private var requestModel = RequestModel()
  private var layoutModel = LayoutModel()
  
  // MARK: - View setup
  func configureView() {
    // Update the user interface for the detail item.
    if let detail = detailItem {
      //title = detail.fullName
      
      if let repositoryName = repositoryName, repositoryDescription = repositoryDescription {
        repositoryName.attributedText = layoutModel.buldRepositoryName(detail.fullName)
        repositoryDescription.text = detail.desc
        coverView.hidden = true
      }
    }
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
    self.configureView()
    
    dataTable.layer.borderColor = UIColor(colorLiteralRed: (229/255), green: (229/255), blue: (229/255), alpha: 1).CGColor
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    dataTable.alpha = 0
    
    if coverView.hidden {
      self.retrieveData(categorySelector.selectedSegmentIndex)
    }
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    
    let tableHeight = dataTable.contentSize.height
    let heightLimit = (self.view.frame.size.height - dataTable.frame.origin.y) - 10
    
    dataTableHeight.constant = tableHeight > heightLimit ? heightLimit : tableHeight
    self.view.layoutIfNeeded()
  }
  
  // MARK: - Segue
  
  override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    if segue.identifier == "showNotice" {
      let notice = segue.destinationViewController as! NoticeViewController
      notice.delegate = self
    }
  }
  
  // MARK: - Data retrieval
  
  func retrieveData(type: Int) {
    isReloading = true
    let urlString = type == 0 ? detailItem?.issueURL : detailItem?.contributorURL
    
    if urlString != "" {
      let url = NSURL(string: urlString!)
      
      if let _ = url {
        requestModel.retrieveData(url!, completion: {
          data, error in
          
          if error != nil {
            // Error retrieving data
            if error!.lowercaseString.rangeOfString("api rate limit exceeded") != nil {
              dispatch_async(dispatch_get_main_queue(), {
                if self.presentedViewController == nil {
                  print("Showing notice")
                  self.performSegueWithIdentifier("showNotice", sender: nil)
                }
              })
            }
            else {
              dispatch_async(dispatch_get_main_queue(), {
                self.errors = ["There's an error while retreiving data from server."]
                self.dataTable.reloadData()
                self.showTable()
              })
            }
          }
          else {
            if type == 0 {
              if let _ = data {
                self.issues = data!
                self.errors = nil
                self.issuesRetrieved = true
              }
              else {
                self.errors = ["No data returned from server."]
              }
            }
            else {
              if let _ = data {
                self.contributors = data!
                self.errors = nil
                self.contributorsRetrieved = true
              }
              else {
                self.errors = ["No data returned from server."]
              }
            }
            
            dispatch_async(dispatch_get_main_queue(), {
              self.dataTable.reloadData()
              self.showTable()
            })
          }
        })
      }
      else {
        // Invalid url provided.
        errors = ["Cannot retrieve data. Invalid URL was provided."]
        dataTable.reloadData()
        self.showTable()
      }
    }
    else {
      // No url for issues/contributors found
      if type == 0 {
        issuesRetrieved = true
      }
      else {
        contributorsRetrieved = true
      }
      
      dataTable.reloadData()
      self.showTable()
    }
  }
  
  func showTable() {
    UIView.animateWithDuration(0.5, animations: {
      self.spinner.alpha = 0
      self.dataTable.alpha = 1
    })
  }
  
  // MARK: - IBAction
  
  @IBAction func selectCategory(sender: UISegmentedControl) {
    spinner.alpha = 1
    dataTable.alpha = 0
    
    if sender.selectedSegmentIndex == 0 {
      if !issuesRetrieved {
        self.retrieveData(sender.selectedSegmentIndex)
      }
      else {
        dataTable.reloadData()
        self.showTable()
      }
    }
    else {
      if !contributorsRetrieved {
        self.retrieveData(sender.selectedSegmentIndex)
      }
      else {
        dataTable.reloadData()
        self.showTable()
      }
    }
  }
}

// MARK: - Table view data source

extension DetailViewController: UITableViewDataSource {
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let _ = errors {
      return errors!.count
    }
    else {
      if categorySelector.selectedSegmentIndex == 0 {
        return issues.count == 0 ? 1 : issues.count
      }
      else {
        return contributors.count == 0 ? 1 : contributors.count
      }
    }
  }
  
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 1
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    //print("cell for row at index path")
    if let error = errors {
      let cell = tableView.dequeueReusableCellWithIdentifier("noContent", forIndexPath: indexPath)
      cell.textLabel?.text = error[indexPath.row] as? String
      return cell
    }
    else {
      if categorySelector.selectedSegmentIndex == 0 {
        if issues.count > 0 {
          let cell = tableView.dequeueReusableCellWithIdentifier("issueContent", forIndexPath: indexPath) as! IssueCell
          let ticketDetails = issues[indexPath.row]
          cell.ticketHeader.text = (ticketDetails["title"] as? String)?.trim()
          cell.ticketMeta.text = layoutModel.buildMetaWithData(ticketDetails as! Dictionary<String, AnyObject>)
          
          if let desc = (ticketDetails["body"] as? String)?.trim() {
            cell.ticketBody.text = desc == "" ? "No description provided." : desc
          }
          else {
            cell.ticketBody.text = "No description provided."
          }
          
          if (ticketDetails["labels"] as! [AnyObject]).count > 0 {
            layoutModel.insertLabels(ticketDetails["labels"] as! [AnyObject], inView: cell.labelContainer)
            cell.labelContainerHeight.constant = 25
            cell.labelContainerBottom.constant = 10
          }
          
          return cell
        }
        else {
          let cell = tableView.dequeueReusableCellWithIdentifier("noContent", forIndexPath: indexPath)
          cell.textLabel?.text = "No issues."
          return cell
        }
      }
      else {
        if contributors.count > 0 {
          let cell = tableView.dequeueReusableCellWithIdentifier("contributorContent", forIndexPath: indexPath) as! ContributorCell
          let contributorDetails = contributors[indexPath.row]
          
          cell.contributor.text = (contributorDetails["login"] as? String)?.trim()
          cell.contributions.text = layoutModel.buildContributionText(contributorDetails as! Dictionary<String, AnyObject>)
          
          if let avatarURL = contributorDetails["avatar_url"] as? String {
            cell.contributorAvatar.downloadedFrom(link: avatarURL, contentMode: .ScaleAspectFit)
          }
          
          return cell
        }
        else {
          let cell = tableView.dequeueReusableCellWithIdentifier("noContent", forIndexPath: indexPath)
          cell.textLabel?.text = "No contributors."
          return cell
        }
      }
    }

  }
}

// MARK: - Table view delegates

extension DetailViewController: UITableViewDelegate {
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }

  func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    
    return 70
  }
  
  func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {

    cell.preservesSuperviewLayoutMargins = false
    cell.separatorInset = UIEdgeInsetsZero
    cell.layoutMargins = UIEdgeInsetsZero
    
    if let lastRow = tableView.indexPathsForVisibleRows?.last?.row {
      if indexPath.row == lastRow {
        isReloading = false
        dataTable.layoutIfNeeded()
        let tableHeight = dataTable.contentSize.height
        let heightLimit = (self.view.frame.size.height - dataTable.frame.origin.y) - 10
        
        dataTableHeight.constant = tableHeight > heightLimit ? heightLimit : tableHeight
        dataTable.layoutIfNeeded()
      }
    }
  }
  
  func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView(frame: CGRectZero)
  }
  
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 0
  }
  
  func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 0
  }
}

// MARK: - Notice view delegate

extension DetailViewController: NoticeViewControllerDelegate {
  func dismissNoticeViewController(controller: NoticeViewController) {
    controller.dismissViewControllerAnimated(true, completion: nil)
    
    // Resume data retrieval
    self.retrieveData(categorySelector.selectedSegmentIndex)
  }
}