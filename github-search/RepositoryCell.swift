//
//  RepositoryCell.swift
//  github-search
//
//  Created by Anna Fortuna on 23/02/2016.
//  Copyright © 2016 AVF. All rights reserved.
//

import Foundation
import UIKit

class RepositoryCell: UITableViewCell {
  
  // MARK: - Properties
  
  @IBOutlet weak var repoNumber: UILabel!
  @IBOutlet weak var repoName: UILabel!
  @IBOutlet weak var repoDescription: UILabel!
}