//
//  NoticeViewController.swift
//  github-search
//
//  Created by Anna Fortuna on 25/02/2016.
//  Copyright © 2016 AVF. All rights reserved.
//

import Foundation
import UIKit

protocol NoticeViewControllerDelegate: class {
  func dismissNoticeViewController(controller: NoticeViewController)
}

class NoticeViewController: UIViewController {
  
  // MARK: - Properties
  @IBOutlet weak var timerDisplay: UILabel!
  var delegate: NoticeViewControllerDelegate?
  private var timer = NSTimer()
  private var counter = 60
  
  // MARK: - View setup
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    timerDisplay.text = "\(counter)"
    timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: "countDown", userInfo: nil, repeats: true)
  }
  
  func countDown() {
    if counter == 0 {
      timer.invalidate()
      delegate?.dismissNoticeViewController(self)
    }
    else {
      --counter;
      timerDisplay.text = "\(counter)"
    }
  }
}