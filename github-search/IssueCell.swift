//
//  IssueCell.swift
//  github-search
//
//  Created by Anna Fortuna on 21/02/2016.
//  Copyright © 2016 AVF. All rights reserved.
//

import Foundation
import UIKit

class IssueCell: UITableViewCell {
  
  // MARK: - Properties
  
  @IBOutlet weak var ticketHeader: UILabel!
  @IBOutlet weak var ticketMeta: UILabel!
  @IBOutlet weak var ticketBody: UILabel!
  @IBOutlet weak var labelContainer: UIScrollView!
  
  @IBOutlet weak var labelContainerHeight: NSLayoutConstraint!
  @IBOutlet weak var labelContainerBottom: NSLayoutConstraint!
  
  override func prepareForReuse() {
    super.prepareForReuse()
    for view in labelContainer.subviews {
      view.removeFromSuperview()
    }
    
    labelContainerHeight.constant = 0
    labelContainerBottom.constant = 0
  }
}