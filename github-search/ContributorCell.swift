//
//  ContributorCell.swift
//  github-search
//
//  Created by Anna Fortuna on 23/02/2016.
//  Copyright © 2016 AVF. All rights reserved.
//

import Foundation
import UIKit

class ContributorCell: UITableViewCell {
  
  // MARK: - Properties
  
  @IBOutlet weak var contributorAvatar: UIImageView!
  @IBOutlet weak var contributor: UILabel!
  @IBOutlet weak var contributions: UILabel!
  
  override func prepareForReuse() {
    super.prepareForReuse()
    
    self.contributorAvatar.image = UIImage(named: "avatar-placeholder")
  }
}