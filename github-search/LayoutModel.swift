//
//  LayoutModel.swift
//  github-search
//
//  Created by Anna Fortuna on 21/02/2016.
//  Copyright © 2016 AVF. All rights reserved.
//

import Foundation
import UIKit

class LayoutModel: NSObject {
  
  func layoutLabels(labels: [AnyObject], inContainer container: UIView) -> [AnyObject] {
    var contentHandler = [AnyObject]()
    var previousLabel: UILabel?
    let labelCount = labels.count
    
    for (var i = 0; i < labelCount; ++i) {
      let isLast = i == (labelCount - 1) ? true : false
      let tag = UILabel(frame: CGRectZero)
      tag.translatesAutoresizingMaskIntoConstraints = false
      tag.text = labels[i]["name"] as? String
      tag.textAlignment = .Center
      tag.layer.cornerRadius = 5
      tag.font = UIFont.systemFontOfSize(12, weight: UIFontWeightSemibold)
      tag.backgroundColor = UIColor(hexString: labels[i]["color"] as! String)
      
      switch ((labels[i]["color"] as! String)) {
        case "000000", "cd19e7", "8c564b", "007700", "009800", "cc317c", "e11d21": tag.textColor = UIColor.whiteColor()
        break
        default: tag.textColor = UIColor.blackColor()
        break
      }
    
      let constraints = self.constraintsForLabel(tag, inView: container, withPreviousLabel: previousLabel, isLast: isLast)
      
      previousLabel = tag
      contentHandler.append([tag, constraints])
    }
    
    return contentHandler
  }
  
  func constraintsForLabel(label: UILabel, inView superView: UIView, withPreviousLabel prevLabel: UILabel?, isLast: Bool) -> [NSLayoutConstraint] {
    
    var constraints = [NSLayoutConstraint]()
    
    let widthConstant = ((label.text! as NSString).boundingRectWithSize(superView.frame.size, options: .UsesLineFragmentOrigin, attributes: [NSFontAttributeName: label.font], context: nil)).size.width
    
    let width = NSLayoutConstraint(item: label, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: widthConstant + 10)
    
    let height = NSLayoutConstraint(item: label, attribute: .Height, relatedBy: .Equal, toItem: superView, attribute: .Height, multiplier: 1, constant: 0)
    
    let centerY = NSLayoutConstraint(item: label, attribute: .CenterY, relatedBy: .Equal, toItem: superView, attribute: .CenterY, multiplier: 1, constant: 0)
    
    var leading = NSLayoutConstraint(item: label, attribute: .Leading, relatedBy: .Equal, toItem: superView, attribute: .Leading, multiplier: 1, constant: 0)
    
    if prevLabel != nil {
      leading = NSLayoutConstraint(item: label, attribute: .Leading, relatedBy: .Equal, toItem: prevLabel, attribute: .Trailing, multiplier: 1, constant: 5)
    }
    
    constraints.append(height)
    constraints.append(width)
    constraints.append(centerY)
    constraints.append(leading)
    
    if isLast {
      let trailing = NSLayoutConstraint(item: label, attribute: .Trailing, relatedBy: .LessThanOrEqual, toItem: superView, attribute: .Trailing, multiplier: 1, constant: -5)
      constraints.append(trailing)
    }
    
    return constraints
  }
  
  func insertLabels(labels: [AnyObject], inView view: UIView) {
    let content = self.layoutLabels(labels, inContainer: view)
    
    for data in content {
      view.addSubview(data[0] as! UILabel)
      view.addConstraints(data[1] as! [NSLayoutConstraint])
    }
  }
  
  func buldRepositoryName(name: String) -> NSAttributedString {
    let newRepositoryName = NSMutableAttributedString(string: name)
    let slashIndex = (name as NSString).rangeOfString("/").location + 1
    
    newRepositoryName.addAttributes([NSForegroundColorAttributeName: UIColor(colorLiteralRed: (64/255), green: (120/255), blue: (192/255), alpha: 1), NSFontAttributeName: UIFont.systemFontOfSize(18)], range: NSMakeRange(0, name.characters.count))
    
    newRepositoryName.addAttributes([NSFontAttributeName: UIFont.systemFontOfSize(18, weight: UIFontWeightBold)], range: NSMakeRange(slashIndex, (name.characters.count - slashIndex)))
    
    return newRepositoryName
  }
  
  func buildMetaWithData(data: Dictionary<String,AnyObject>) -> String {
    var metaString = "#\(data["number"] as! Int) opened"

    if let createdAt = data["created_at"] as? String {
      let dateFormatter = NSDateFormatter()
      dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
      let createdAtDate = dateFormatter.dateFromString(createdAt)
      let dateDiff = NSCalendar.currentCalendar().components(.Second, fromDate: createdAtDate!, toDate: NSDate(), options: []).second
      
      if dateDiff > 60 {
        let min = dateDiff / 60
        
        if min > 60 {
          let hr = min / 60
          
          if hr > 24 {
            let day = hr / 24
            metaString += " \(day) "
            metaString += day > 1 ? "days ago" : "day ago"
          }
          else {
            metaString += " \(hr) "
            metaString += hr > 1 ? "hours ago" : "hour ago"
          }
        }
        else {
          metaString += " \(min) "
          metaString += min > 1 ? "minute ago" : "minutes ago"
        }
      }
      else {
        metaString += " \(dateDiff) "
        metaString += dateDiff > 1 ? "seconds ago" : "second ago"
      }
    }
    
    if let userDetails = data["user"] as? Dictionary<String,AnyObject> {
      if let userLogin = userDetails["login"] as? String {
        metaString += " by \(userLogin)"
      }
      else {
        metaString += " by Anonymous"
      }
    }
    
    return metaString
  }
  
  func buildContributionText(data: Dictionary<String,AnyObject>) -> String {
    var contributions = "\(data["contributions"] as! Int) contribution"
    if (data["contributions"] as! Int) > 1 {
      contributions += "s"
    }
    
    return contributions
  }

}