//
//  RequestModel.swift
//  github-search
//
//  Created by Anna Fortuna on 21/02/2016.
//  Copyright © 2016 AVF. All rights reserved.
//

import Foundation

class RequestModel: NSObject {
  
  private var page: Int
  private var numOfPages: Int
  private var domain: String
  private var goSearch: Bool
  private var isSearching: Bool
  
  var searchKeyword: String {
    didSet {
      if searchKeyword != oldValue {
        self.goSearch = true
        self.page = 1
        self.numOfPages = 0
      }
    }
  }
  
  override init() {
    self.page = 1
    self.numOfPages = 0
    self.goSearch = true
    self.domain = "https://api.github.com"
    self.searchKeyword = ""
    self.isSearching = false
    
    super.init()
  }
  
  func searchRepositories(keyword: String, completion: (data: [Repository]?, error: String?) -> ()) {
    if !self.isSearching {
      if self.goSearch {
        self.isSearching = true
        let encodedKeyword = keyword.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        
        let url = NSURL(string: "https://api.github.com/search/repositories?q=language:\(encodedKeyword!)&sort=stars&page=\(self.page)")
        
        if let _ = url {
          let urlRequest = NSURLRequest(URL: url!)
          let session = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest, completionHandler: {
            data, response, error in
            
            if error != nil {
              self.isSearching = false
              completion(data: nil, error: "\(error!.localizedDescription)")
            }
            else {
              if let _ = data {
                do {
                  let jsonData = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
                  if let totalCount = jsonData["total_count"] as? Int {
                    
                    // Github displays up to 1000 results only.
                    self.numOfPages = (totalCount > 1000) ? 34 : Int(ceil(Double(totalCount / 30)))
                    
                    if let items = jsonData["items"] as? [Dictionary<String,AnyObject>] {
                      var itemList = [Repository]()
                      
                      for item in items {
                        let repo = Repository()
                        repo.name = item["name"] as! String
                        repo.fullName = item["full_name"] as! String
                        repo.desc = item["description"] as? String
                        repo.issueURL = (item["issues_url"] as! String).stringByReplacingOccurrencesOfString("{/number}", withString: "")
                        repo.contributorURL = item["contributors_url"] as! String
                        
                        itemList.append(repo)
                      }
                      
                      if self.page == self.numOfPages { // If we've reached the last page.
                        self.page = 1
                        self.numOfPages = 0
                        self.goSearch = false
                      }
                      else { // Otherwise, on to the next one.
                        self.page += 1
                      }
                      
                      self.isSearching = false
                      completion(data: itemList, error: nil)
                    }
                    else {
                      // No items were returned, so we can no longer retrieve more items from this search.
                      self.goSearch = false
                      self.isSearching = false
                      completion(data: nil, error: nil)
                    }
                  }
                  else {
                    self.isSearching = false
                    var errorMessage = "Unknown error occurred while retrieving data."
                    
                    if let message = jsonData["message"] as? String {
                      errorMessage = "\(message)\n\n"
                    }
                    
                    if let error = jsonData["errors"] as? [Dictionary<String,String>] {
                      if error.count > 0 {
                        errorMessage += error[0]["message"]! as String
                      }
                    }
                    
                    completion(data: nil, error: errorMessage)
                  }
                }
                catch {
                  self.isSearching = false
                  completion(data: nil, error: "Unable to properly process data received from server. Please try again.")
                }
              }
              else {
                self.isSearching = false
                completion(data: nil, error: "No data received from server. Please try again.")
              }
            }
          })
          
          session.resume()
        }
        else {
          self.isSearching = false
          completion(data: nil, error: "It seems like you've entered an invalid character. Please clean up your keyword and try again!")
        }
      }
    }
  }
  
  func retrieveData(url: NSURL, completion:(data: [AnyObject]?, error: String?) -> ()) {
    let urlRequest = NSURLRequest(URL: url)
    
    let session = NSURLSession.sharedSession().dataTaskWithRequest(urlRequest, completionHandler: {
      data, response, error in
      
      if error != nil {
        completion(data: nil, error: "Error retrieiving data: \(error)")
      }
      else {
        if let _ = data {
          do {
            var dataArray = [AnyObject]()
            let jsonData = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments)
            
            if let parsedData = jsonData as? [AnyObject] {
              if parsedData.count > 0 {
                let limit = parsedData.count > 3 ? 3 : parsedData.count
                
                for (var i = 0; i < limit; ++i) {
                  dataArray.append(parsedData[i])
                }
              }
              
              completion(data: dataArray, error: nil)
            }
            else {
              if let message = jsonData["message"] as? String {
                completion(data: nil, error: message)
              }
              else {
                completion(data: nil, error: "Unknown error.")
              }
            }
          }
          catch {
            completion(data: nil, error: "Error parsing json: \(error)")
          }
        }
        else {
          completion(data: nil, error: nil)
        }
      }
    })
    
    session.resume()
  }
}